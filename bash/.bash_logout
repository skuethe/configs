# ssh-agent specific
# kill ssh-agent on logout - only if it exists and we are NOT in tmux session (otherwise it will kill on each window / pane close)
if [[ ${SSH_AGENT_PID+1} == 1 ]] && [[ -z "${TMUX}" ]]; then
    ssh-add -D
    ssh-agent -k > /dev/null 2>&1
    unset SSH_AGENT_PID
    unset SSH_AUTH_SOCK
fi