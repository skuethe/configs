# WSL2 specific
# if docker integration is active (otherwhise it will override the installed kubectl and use an old version)
alias kubectl="/usr/bin/kubectl"

# Kubectl specific
alias k="kubectl"
alias kp="kubectl get pods"
alias kdp="kubectl get deployments"
alias ksts="kubectl get statefulset"
alias kl="kubectl logs --tail=100"
alias kla="kubectl logs"
alias klf="kubectl logs --tail=100 -f"
alias klfa="kubectl logs -f"
alias kd="kubectl describe"
alias ke="kubectl exec -it"
alias kall="kubectl api-resources --verbs=list --namespaced -o name | xargs -n 1 kubectl get --show-kind --ignore-not-found -n"
alias kc="kubectl config view -o json | jq -r '.contexts[].name'"
alias kcu="kubectl config use-context"
alias kn="kubectl config set-context --current --namespace"

# Others
alias whatismyip="curl ifconfig.me; echo -en '\n'"
alias starwars="telnet towel.blinkenlights.nl"
alias matrix='tr -c "[:digit:]" " " < /dev/urandom | dd cbs=$COLUMNS conv=unblock | GREP_COLOR="1;32" grep --color "[^ ]" 2>/dev/null'
alias typeit="pv -qL 10"

# Tmux specific
# override default command from "new-session" to "attach", as we are starting session and creating windows inside .tmux.conf
#alias tmux="tmux attach"