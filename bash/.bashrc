# Kubectl completion
# Use this rather than /etc/bash_completion.d/[kubectl] file so we can respect changes after new kubectl versions are installed
# Also, make completion work with "k" alias
source <(kubectl completion bash)
complete -F __start_kubectl k

# ssh-agent specific
# start an ssh-agent (if not already done) and add our - only do so if we are NOT in tmux session (otherwise it will start on each new window / pane)
if [[ -z "${TMUX}" ]]; then
  source <(ssh-agent)
  ssh-add
fi